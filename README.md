# JavaScript React JSON Placeholder
Simple React app that calls to https://jsonplaceholder.typicode.com/users/{id}

# Get Started
- Clone this repo
- Install dependencies: `npm install`
- Run app: `npm start`

# Deploy
`aws s3 sync ./build s3://react-json-placeholder`
[Website](http://react-json-placeholder.s3-website-us-east-1.amazonaws.com)
