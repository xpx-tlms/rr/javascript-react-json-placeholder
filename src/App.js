import axios from 'axios'
import { useRef, useState } from 'react'
import './App.css'

function App() {
  let inputRef = useRef();
  let [userData, setUserData] = useState(null)

  const onHandleClick = async () => {
    if (inputRef.current.value === "") {
      alert("Please enter user id 1-10.")
      return
    }
    const res = await axios.get(`https://jsonplaceholder.typicode.com/users/${inputRef.current.value}`)
    setUserData(JSON.stringify(res.data))
    inputRef.current.value = ""
  }

  return (
    <div className="App">
      Get User: https://jsonplaceholder.typicode.com/users/ <br/>
      <input placeholder="User Id (1-10)" ref={inputRef} type="text"></input>
      <button onClick={onHandleClick}>Submit</button><br/>
      {userData}
    </div>
  )
}

export default App;
